A simple tic-tac-toe implementation in javafx, including AI.

To start the game clone the repository and run via Gradle.

`git clone https://gitlab.com/Pinusar/java-tic-tac-toe.git`

`cd java-tic-tac-toe`

`./gradlew run`
