package engine;

import java.util.HashMap;

public class Board {

    private HashMap<Integer, String> state;

    public Board() {
        this.state = state = new HashMap<>();
        state.put(1, ".");
        state.put(2, ".");
        state.put(3, ".");
        state.put(4, ".");
        state.put(5, ".");
        state.put(6, ".");
        state.put(7, ".");
        state.put(8, ".");
        state.put(9, ".");
    }

    public Board(HashMap<Integer, String> state) {
        this.state = state;
    }

    public HashMap<Integer, String> getState() {
        return state;
    }
    
    public String displayAsText() {
        return String.format("+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+", state.get(1), state.get(2), state.get(3), state.get(4),
                state.get(5), state.get(6), state.get(7), state.get(8), state.get(9));
    }
}
