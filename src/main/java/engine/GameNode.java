package engine;

import java.util.ArrayList;
import java.util.List;

public class GameNode {
    private Integer targetTile;
    private Integer evaluationScore;
    private Board board;
    private String boardState;
    private List<GameNode> children;
    private List<Integer> previousMoves;

    public GameNode() {
        this.children = new ArrayList<>();
        this.previousMoves = new ArrayList<>();
    }

    public Integer getTargetTile() {
        return targetTile;
    }

    public void setTargetTile(Integer targetTile) {
        this.targetTile = targetTile;
    }

    public Integer getEvaluationScore() {
        return evaluationScore;
    }

    public void setEvaluationScore(Integer evaluationScore) {
        this.evaluationScore = evaluationScore;
    }

    public List<GameNode> getChildren() {
        return children;
    }

    public String getBoardState() {
        return boardState;
    }

    public void setBoardState(String boardState) {
        this.boardState = boardState;
    }

    public void setChildren(List<GameNode> children) {
        this.children = children;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<Integer> getPreviousMoves() {
        return previousMoves;
    }
}
