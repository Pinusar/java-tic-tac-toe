package sample;


import engine.BoardEvaluator;
import engine.Game;
import engine.GameNode;
import engine.IllegalMoveException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.io.IOException;


public class Controller {

    @FXML
    public Label message;
    private final Game game = new Game();
    private final BoardEvaluator evaluator = new BoardEvaluator("O");

    public void placeMove(ActionEvent event) throws InterruptedException {
        //player move
        Button button = (Button) event.getSource();
        String targetTile = button.getId();
        String playerMark = game.getActivePlayer().getMark();
        try {game.playTurn(Integer.parseInt(targetTile));}
        catch (IllegalMoveException e) {
            System.out.println("Bad move!");
            message.setText("Bad move! Try again!");
            return;
        }
        button.setText(playerMark);

        //computer move
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        try {
            Integer computerTargetTile = evaluator.findNextMove(root, "O").getTargetTile();
            game.playTurn(computerTargetTile);
            Button computerTargetButton = (Button) button.getScene().lookup("#" + computerTargetTile);
            computerTargetButton.setText("O");
        }
        catch (RuntimeException e) {
            System.out.println("Computer move failed.");
        }

        if (game.isOver()) {
            if (BoardEvaluator.isBoardFull(game.getBoard())) {
                message.setText("It's a draw!");
            } else {
                message.setText(String.format("Player %s won!", game.getWinner()));
            }
        }
    }

    public void restart(ActionEvent event) throws IOException {
        Button button = (Button) event.getSource();
        Scene scene = button.getScene();
        scene.setRoot(FXMLLoader.load(Main.class.getResource("/fxml/sample.fxml")));
    }
}
